<?php
/**
 * CoososVWorkflowBundle.php
 *
 * @author Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CoososVWorkflowBundle
 *
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class CoososVWorkflowBundle extends Bundle
{
}
