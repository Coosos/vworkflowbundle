<?php
/**
 * Serializer
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service;

use Coosos\VWorkflowBundle\Event\PreSerializerEvent;
use Coosos\VWorkflowBundle\Event\SerializerConfigurationEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class Serializer
 *
 * @package Coosos\VWorkflowBundle\Service
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class Serializer implements SerializerInterface
{
    /**
     * @var \Symfony\Component\Serializer\Serializer serializer
     */
    private $serializer;

    /**
     * @var null|EventDispatcherInterface dispatcher
     */
    private $dispatcher;

    /**
     * Serializer constructor.
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->serializer = $this->configureSerializer();
    }

    /**
     * @return \Symfony\Component\Serializer\Serializer
     */
    protected function configureSerializer()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $event = new SerializerConfigurationEvent($normalizer, $encoder);
        $this->dispatcher->dispatch(SerializerConfigurationEvent::EVENT_NAME, $event);

        return new \Symfony\Component\Serializer\Serializer(array($normalizer), array($encoder));
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($data, $format, array $context = [])
    {
        $event = new PreSerializerEvent($data, $format, $context);
        $this->dispatcher->dispatch(PreSerializerEvent::EVENT_NAME, $event);

        return $this->serializer->serialize($data, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function deserialize($data, $type, $format, array $context = [])
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }
}
