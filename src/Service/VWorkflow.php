<?php
/**
 * VWorkflow Service
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service;

/**
 * Class VWorkflow
 *
 * @package Coosos\VWorkflowBundle\Service
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 *
 * @deprecated Use \Coosos\VWorkflowBundle\Service\VWorkflowService
 */
class VWorkflow extends VWorkflowService
{
}
