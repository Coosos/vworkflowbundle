<?php
/**
 * CreateEntity
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service\VWorkflow;

use Coosos\VWorkflowBundle\Entity\VWorkflow;
use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Coosos\VWorkflowBundle\Service\Serializer;

/**
 * Class CreateEntity
 *
 * @package Coosos\VWorkflowBundle\Service\VWorkflow
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class CreateEntity
{
    /**
     * @var Serializer serializer
     */
    private $serializer;

    /**
     * @var Configuration configuration
     */
    private $configuration;

    /**
     * CreateEntity constructor.
     *
     * @param Configuration $configuration
     * @param Serializer    $serializer
     */
    public function __construct(Configuration $configuration, Serializer $serializer)
    {
        $this->configuration = $configuration;
        $this->serializer = $serializer;
    }

    /**
     * @param VWorkflowTrait $model
     * @param array          $params
     * @return VWorkflow
     */
    public function create($model, $params = [])
    {
        if (!isset($params['serializer'])) {
            $params['serializer'] = [];
        }

        $modelCloned = clone $model;
        $modelCloned->vworkflowModel = null;

        $vworkflow = new VWorkflow();
        $vworkflow->setOld(($model->vworkflowModel) ? $model->vworkflowModel : null);
        $vworkflow->setMarking($model->getMarking());
        $vworkflow->setObjectSerialized($this->serializer->serialize($modelCloned, 'json', $params['serializer']));
        $vworkflow->setEntityClass(get_class($model));
        $vworkflow->setMarking($model->getMarking());
        $vworkflow->setWorkflowName($model->workflowName);

        $vworkflowConfiguration = $this->configuration->getByWorkflowName($model->workflowName);

        if ($model->getMarking()) {
            if (is_array($model->getMarking())) {
                foreach ($model->getMarking() as $status => $value) {
                    if ($vworkflowConfiguration->isStatusAutoMerge($status)) {
                        $vworkflow->setIsMerged(true);
                        break;
                    }
                }
            } elseif (is_string($model->getMarking())) {
                if ($vworkflowConfiguration->isStatusAutoMerge($model->getMarking())) {
                    $vworkflow->setIsMerged(true);
                }
            }
        }

        $instance = spl_object_hash($vworkflow);
        $vworkflow->setInstance($instance);

        return $vworkflow;
    }
}
