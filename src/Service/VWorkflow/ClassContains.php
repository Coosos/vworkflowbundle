<?php
/**
 * ClassContains
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service\VWorkflow;

/**
 * Class ClassContains
 *
 * @package Coosos\VWorkflowBundle\Service\VWorkflow
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class ClassContains
{
    /**
     * Return true if the given object has the given method, false if not
     *
     * @param \ReflectionClass $class
     * @param string           $methodName
     * @return bool
     */
    public function hasMethod(\ReflectionClass $class, $methodName)
    {
        return $class->hasMethod($methodName);
    }

    /**
     * Return true if the given object use the given trait, false if not
     *
     * @param \ReflectionClass|mixed $class
     * @param string                 $traitName
     * @param boolean                $isRecursive
     * @return bool
     * @throws \ReflectionException
     */
    public function hasTrait($class, $traitName, $isRecursive = false)
    {
        if (!($class instanceof \ReflectionClass)) {
            $entityClass = get_class($class);
            $class = new \ReflectionClass($entityClass);
        }

        if (in_array($traitName, $class->getTraitNames())) {
            return true;
        }

        $parentClass = $class->getParentClass();

        if (($isRecursive === false) || ($parentClass === false) || ($parentClass === null)) {
            return false;
        }

        return $this->hasTrait($parentClass, $traitName, $isRecursive);
    }

    /**
     * Return true if the given object has the given property, false if not
     *
     * @param \ReflectionClass $class
     * @param string           $propertyName
     * @return bool
     */
    public function hasProperty(\ReflectionClass $class, $propertyName)
    {
        if ($class->hasProperty($propertyName)) {
            return true;
        }

        $parentClass = $class->getParentClass();

        if ($parentClass === false) {
            return false;
        }

        return $this->hasProperty($parentClass, $propertyName);
    }
}
