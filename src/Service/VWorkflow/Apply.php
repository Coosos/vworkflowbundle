<?php
/**
 * Apply
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service\VWorkflow;

use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Symfony\Component\Workflow\Workflow;
use Coosos\VWorkflowBundle\Entity\VWorkflow as VWorkflowEntity;

/**
 * Class Apply
 *
 * @package Coosos\VWorkflowBundle\Service\VWorkflow
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class Apply
{
    /**
     * @var ClassContains classContains
     */
    private $classContains;

    /**
     * @var CreateEntity createEntity
     */
    private $createEntity;

    /**
     * @var Configuration configuration
     */
    private $configuration;

    /**
     * Apply constructor.
     *
     * @param ClassContains $classContains
     * @param CreateEntity  $createEntity
     * @param Configuration $configuration
     */
    public function __construct(
        ClassContains $classContains,
        CreateEntity $createEntity,
        Configuration $configuration
    ) {
        $this->classContains = $classContains;
        $this->createEntity = $createEntity;
        $this->configuration = $configuration;
    }

    /**
     * Process to apply entity with VWorkflow
     *
     * @param VWorkflowTrait|mixed $model
     * @param string|array|null    $transitions
     * @param Workflow             $workflowService
     * @param array                $params
     * @return VWorkflowEntity|VWorkflowEntity[]
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function apply($model, $workflowService, $transitions = null, $params = [])
    {
        if (!($model instanceof VWorkflowEntity)) {
            if (!$this->modelHasTrait($model)) {
                throw new \Exception('Your model has no VWorkflowTrait');
            }
        }

        $ignoreWorkflowCanParam = (isset($params['ignoreWorkflowCan'])) ? $params['ignoreWorkflowCan'] : false;
        $ignoreWorkflowApplyParam = (isset($params['ignoreWorkflowApply'])) ? $params['ignoreWorkflowApply'] : false;

        if ($ignoreWorkflowApplyParam && (!isset($params['marking']) || empty($params['marking']))) {
            throw new \Exception(
                'If use ignore apply workflow parameter, you should specifique marking in parameter !'
            );
        }

        if (isset($params['marking'])) {
            $model->setMarking($params['marking']);
        }

        if (is_null($transitions) || is_string($transitions)) {
            if (!is_null($transitions) && !$ignoreWorkflowCanParam && !$workflowService->can($model, $transitions)) {
                throw new \Exception('Can\'t to apply transition !');
            }

            if (!is_null($transitions) && !$ignoreWorkflowApplyParam) {
                $workflowService->apply($model, $transitions);
            }

            return $this->create($model, $workflowService, $params);
        }

        /** @var VWorkflowEntity[] $vworkflowList */
        $vworkflowList = [];
        foreach ($transitions as $transition) {
            if (!$ignoreWorkflowCanParam && !$workflowService->can($model, $transition)) {
                throw new \Exception('Can\'t to apply transition !');
            }

            if (!$ignoreWorkflowApplyParam) {
                $workflowService->apply($model, $transition);
            }

            $vworkflow = $this->create($model, $workflowService, $params);
            if (count($vworkflowList) > 0) {
                $vworkflow->setOld($vworkflowList[count($vworkflowList) - 1]);
            }

            $vworkflowList[] = $vworkflow;
        }

        return $vworkflowList;
    }

    /**
     * Clone object with relation
     *
     * @param VWorkflowTrait $model
     * @param array          $already
     * @return VWorkflowTrait
     * @throws \ReflectionException
     */
    protected function cloneObject($model, &$already = [])
    {
        if (in_array(spl_object_hash($model), $already)) {
            if (method_exists($model, 'get' . ucfirst($this->identifier()))) {
                return $model->{'get' . ucfirst($this->identifier())}();
            }

            return $model;
        }

        $already[] = spl_object_hash($model);

        $modelCloned = clone $model;
        $reflection = new \ReflectionClass(get_class($modelCloned));
        $properties = $reflection->getProperties();
        $ignoreProperty = ['vworkflowModel', 'workflowAction', 'vworkflowUniqId', 'marking', 'workflowName'];
        foreach ($properties as $property) {
            if (in_array($property->getName(), $ignoreProperty)) {
                continue;
            }

            if (method_exists($modelCloned, 'get' . ucfirst($property->getName()))
                && (is_object($modelCloned->{'get' . ucfirst($property->getName())}())
                || is_array($modelCloned->{'get' . ucfirst($property->getName())}()))
            ) {
                if (is_array($modelCloned->{'get' . ucfirst($property->getName())}())
                    || $modelCloned->{'get' . ucfirst($property->getName())}() instanceof \ArrayAccess
                ) {
                    $list = [];
                    foreach ($modelCloned->{'get' . ucfirst($property->getName())}() as $item) {
                        $list[] = $this->cloneObject($item, $already);
                    }

                    $modelCloned->{'set' . ucfirst($property->getName())}($list);
                } elseif (is_object($modelCloned->{'get' . ucfirst($property->getName())}())) {
                    $sModel = $modelCloned->{'get' . ucfirst($property->getName())}();
                    $cloneResult = $this->cloneObject($sModel, $already);

                    $reflectProperty = new \ReflectionProperty(get_class($modelCloned), $property->getName());
                    $reflectProperty->setAccessible(true);
                    $reflectProperty->setValue($modelCloned, $cloneResult);
                }
            }
        }

        return $modelCloned;
    }

    /**
     * Check if marking is correspond for auto merge
     *
     * @param string            $workflowName
     * @param string|array|null $markings
     * @return bool
     * @throws \Exception
     */
    protected function autoMerge($workflowName, $markings)
    {
        $vworkflowConfiguration = $this->configuration->getByWorkflowName($workflowName);
        if (is_null($vworkflowConfiguration)) {
            throw new \Exception('Configuration for ' . $workflowName . ' workflow not found !');
        }

        if (is_string($markings)) {
            return $vworkflowConfiguration->isStatusAutoMerge($markings);
        }

        if (is_array($markings)) {
            foreach (array_keys($markings) as $marking) {
                if ($vworkflowConfiguration->isStatusAutoMerge($marking)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if model has VWorkflowTrait
     *
     * @param VWorkflowTrait|mixed $model
     * @return bool
     * @throws \ReflectionException
     */
    protected function modelHasTrait($model)
    {
        return $this->classContains->hasTrait($model, VWorkflowTrait::class);
    }

    /**
     * Prepare & Create vworkflow entity
     *
     * @param VWorkflowTrait $model
     * @param Workflow       $workflowService
     * @param array          $params
     * @return VWorkflowEntity
     * @throws \ReflectionException
     * @throws \Exception
     */
    protected function create($model, $workflowService, $params)
    {
        $model->vworkflowGenerateUniqId();
        $model->workflowName = $workflowService->getName();
        $modelCloned = $this->cloneObject($model);
        $vworkflow = $this->createEntity->create($modelCloned, $params);

        if ($this->autoMerge($workflowService->getName(), $vworkflow->getMarking())) {
            $vworkflow->setObjectDeserialized($model);
        }

        return $vworkflow;
    }

    /**
     * Identifier model
     *
     * @return string
     */
    protected function identifier()
    {
        return 'id';
    }
}
