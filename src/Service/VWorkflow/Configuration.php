<?php
/**
 * Configuration.php
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service\VWorkflow;

use Coosos\VWorkflowBundle\Model\Configuration\WorkflowConfiguration;

/**
 * Class Configuration
 *
 * @package Coosos\VWorkflowBundle\Service\VWorkflow
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class Configuration
{
    /**
     * @var WorkflowConfiguration[] workflows
     */
    private $workflows;

    /**
     * Configuration constructor.
     *
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        $this->workflows = [];

        if (isset($configuration['workflows'])) {
            foreach ($configuration['workflows'] as $name => $workflow) {
                $this->workflows[] = new WorkflowConfiguration($name, $workflow);
            }
        }
    }

    /**
     * @param string $name
     * @return WorkflowConfiguration|mixed|null
     */
    public function getByWorkflowName(string $name)
    {
        foreach ($this->workflows as $workflow) {
            if ($name === $workflow->getName()) {
                return $workflow;
            }
        }

        return null;
    }
}
