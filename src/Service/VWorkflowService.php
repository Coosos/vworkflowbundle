<?php
/**
 * VWorkflowService
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Service;

use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Coosos\VWorkflowBundle\Service\VWorkflow\Apply;
use Symfony\Component\Workflow\Workflow;
use Coosos\VWorkflowBundle\Entity\VWorkflow as VWorkflowEntity;

/**
 * Class VWorkflowService
 *
 * @package Coosos\VWorkflowBundle\Service
 * @author  Remy Lescallier <lescallier1@gmail.com>
 */
class VWorkflowService
{
    /**
     * @var Apply
     */
    protected $apply;

    /**
     * VWorkflowService constructor.
     *
     * @param Apply $apply
     */
    public function __construct(Apply $apply)
    {
        $this->apply = $apply;
    }

    /**
     * Process to apply entity with VWorkflow
     *
     * @param VWorkflowTrait|mixed $model
     * @param Workflow             $workflowService
     * @param string|array|null    $transition
     * @param array                $params
     * @return VWorkflowEntity|VWorkflowEntity[]
     * @throws \ReflectionException
     */
    public function apply($model, $workflowService, $transition = null, $params = [])
    {
        return $this->apply->apply($model, $workflowService, $transition, $params);
    }
}
