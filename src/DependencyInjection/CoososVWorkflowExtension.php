<?php
/**
 * CoososVWorkflowExtension
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\DependencyInjection;

use Coosos\VWorkflowBundle\Service\VWorkflow\Configuration as VWorkflowConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class CoososVWorkflowExtension
 *
 * @package Coosos\VWorkflowBundle\DependencyInjection
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class CoososVWorkflowExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = $this->processConfiguration(new Configuration(), $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definition = new Definition(VWorkflowConfiguration::class);
        $definition->addArgument($config);
        $container->setDefinition(VWorkflowConfiguration::class, $definition);

        $this->serializerConfigurationService($container);
    }

    /**
     * Auto import service
     *
     * @param ContainerBuilder $containerBuilder
     */
    protected function serializerConfigurationService(ContainerBuilder $containerBuilder)
    {
        $iter = $this->getClassIterator(__DIR__ . '/../EventSubscriber/VWorkflow/SerializerConfiguration');

        /** @var \ReflectionClass $class */
        foreach ($iter as $class) {
            $serviceClassName = str_replace(
                'Coosos\VWorkflowBundle\EventSubscriber\VWorkflow\PreSerializer\\',
                '',
                $class->getName()
            );

            $serviceName = '';
            $serviceClassName = preg_split('/(?=[A-Z])/', $serviceClassName, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($serviceClassName as $value) {
                $serviceName .= strtolower($value) . '_';
            }

            $serviceName = substr($serviceName, 0, -1);

            $containerBuilder
                ->register('coosos.v_workflow.serializer_configuration.' . $serviceName, $class->getName())
                ->addTag('kernel.event_subscriber');
        }
    }

    /**
     * Get class iterator
     *
     * @param string $path
     * @return \hanneskod\classtools\Iterator\ClassIterator
     */
    private function getClassIterator($path)
    {
        $finder = new \Symfony\Component\Finder\Finder();
        $iter = new \hanneskod\classtools\Iterator\ClassIterator($finder->in($path));
        $iter->enableAutoloading();

        return $iter;
    }
}
