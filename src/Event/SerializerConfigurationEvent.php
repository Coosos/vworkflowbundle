<?php
/**
 * SSerializerEvent.php
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class SerializerConfigurationEvent extends Event
{
    const EVENT_NAME = 'coosos.v_workflow.serializer_configuration';

    /**
     * @var AbstractNormalizer normalizer
     */
    private $normalizer;

    /**
     * @var EncoderInterface encoder
     */
    private $encoder;

    /**
     * PreSerializerEvent constructor.
     *
     * @param AbstractNormalizer $normalizer
     * @param EncoderInterface    $encoder
     */
    public function __construct(AbstractNormalizer $normalizer, EncoderInterface $encoder)
    {

        $this->normalizer = $normalizer;
        $this->encoder = $encoder;
    }

    /**
     * @return AbstractNormalizer
     */
    public function getNormalizer()
    {
        return $this->normalizer;
    }

    /**
     * @return EncoderInterface
     */
    public function getEncoder()
    {
        return $this->encoder;
    }
}
