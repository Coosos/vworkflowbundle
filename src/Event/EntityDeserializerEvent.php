<?php
/**
 * EntityDeserializerEvent.php
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Event;


use Symfony\Component\EventDispatcher\Event;

class EntityDeserializerEvent extends Event
{
    const EVENT_NAME = 'coosos.entity_deserializer';

    /**
     * @var array data
     */
    private $data;

    /**
     * @var mixed model
     */
    private $model;

    /**
     * EntityDeserializerEvent constructor.
     *
     * @param array $data
     * @param mixed $model
     */
    public function __construct($data, $model)
    {
        $this->data = $data;
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
}
