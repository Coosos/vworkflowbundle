<?php
/**
 * PreSerializerEvent
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class PreSerializerEvent
 *
 * @package Coosos\VWorkflowBundle\Event
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class PreSerializerEvent extends Event
{
    const EVENT_NAME = 'coosos.v_workflow.pre_serializer';

    /**
     * @var mixed model
     */
    private $model;

    /**
     * @var string serializerFormat
     */
    private $serializerFormat;

    /**
     * @var array serializerContext
     */
    private $serializerContext;

    /**
     * PreSerializerEvent constructor.
     *
     * @param mixed  $model
     * @param string $serializerFormat
     * @param array  $serializerContext
     */
    public function __construct($model, $serializerFormat, $serializerContext)
    {
        $this->model = $model;
        $this->serializerFormat = $serializerFormat;
        $this->serializerContext = $serializerContext;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getSerializerFormat()
    {
        return $this->serializerFormat;
    }

    /**
     * @return array
     */
    public function getSerializerContext()
    {
        return $this->serializerContext;
    }
}
