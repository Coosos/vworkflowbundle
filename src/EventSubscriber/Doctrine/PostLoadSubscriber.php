<?php
/**
 * PostLoadSubscriber
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber\Doctrine;

use Coosos\VWorkflowBundle\Entity\VWorkflow;
use Coosos\VWorkflowBundle\Event\EntityDeserializerEvent;
use Coosos\VWorkflowBundle\EventSubscriber\AbstractSubscriber;
use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Coosos\VWorkflowBundle\Service\VWorkflow\ClassContains;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class PostLoadSubscriber
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber\Doctrine
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class PostLoadSubscriber extends AbstractSubscriber implements EventSubscriber
{

    /**
     * @var SerializerInterface serializer
     */
    private $serializer;

    /**
     * @var EventDispatcherInterface eventDispatcher
     */
    private $eventDispatcher;

    /**
     * PostLoadSubscriber constructor.
     *
     * @param ClassContains            $classContains
     * @param SerializerInterface      $serializer
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        ClassContains $classContains,
        SerializerInterface $serializer,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct($classContains);
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return ['postLoad'];
    }

    /**
     * @param LifecycleEventArgs $args
     * @return PostLoadSubscriber
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        if (!($args->getEntity() instanceof VWorkflow)) {
            return $this;
        }

        /** @var VWorkflow $entity */
        $entity = $args->getEntity();
        $data = json_decode($entity->getObjectSerialized(), true);
        $entityClass = $entity->getEntityClass();
        $object = new $entityClass();

        $dataDeserialized = $this->deserialized($args->getEntityManager(), $data, $object);
        $entity->setObjectDeserialized($dataDeserialized);
        $dataDeserialized->setVworkflowModel($entity);

        return $this;
    }

    /**
     * @param EntityManager        $entityManager
     * @param array                $data
     * @param mixed|VWorkflowTrait $model
     * @return VWorkflowTrait|mixed|null
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function deserialized($entityManager, $data, $model)
    {
        $metadata = $entityManager->getClassMetadata(get_class($model));

        // TODO
        if (count($metadata->getIdentifier()) > 1) {
            return null;
        }

        $idLabel = $metadata->getIdentifier()[0];
        if ((is_array($data) && $data[$idLabel]) || (is_int($data) && !empty($idLabel))) {
            $id = (is_array($data)) ? $data[$idLabel] : $data;
            $find = $entityManager->getRepository(get_class($model))->find($id);
            if ($find) {
                $model = $find;
            }
        }

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (in_array($key, $metadata->getAssociationNames())) {
                    if (method_exists($model, 'get' . ucfirst($key))) {
                        if ($model->{'get' . ucfirst($key)}() instanceof PersistentCollection
                            || $model->{'get' . ucfirst($key)}() instanceof ArrayCollection) {
                            /** @var PersistentCollection|ArrayCollection $collection */
                            $collection = $model->{'get' . ucfirst($key)}();
                            $assoMapping = $metadata->getAssociationMapping($key);
                            $targetEntity = $assoMapping['targetEntity'];

                            $unknownIdCollection = [];
                            $collectionWithId = [];

                            if (!empty($data[$key])) {
                                foreach ($data[$key] as $dataRelation) {
                                    if (is_null($dataRelation['id'])) {
                                        $unknownIdCollection[] = $dataRelation;
                                        continue;
                                    }

                                    $collectionWithId[$dataRelation['id']] = $dataRelation;
                                }
                            } else {
                                $model->{'set' . ucfirst($key)}([]);
                            }

                            if (!empty($collectionWithId)) {
                                /** @var mixed $item */
                                foreach ($collection as $item) {
                                    if (isset($collectionWithId[$item->getId()])) {
                                        $this->deserialized($entityManager, $collectionWithId[$item->getId()], $item);
                                        unset($collectionWithId[$item->getId()]);
                                    } elseif (!empty($item->getId())) {
                                        $collection->removeElement($item);
                                    }
                                }
                            }

                            foreach ($collectionWithId as $id => $item) {
                                $targetEntityObject = new $targetEntity();
                                $targetEntityObject = $this->deserialized($entityManager, $item, $targetEntityObject);
                                $model->{'get' . ucfirst($key)}()->add($targetEntityObject);
                            }

                            foreach ($unknownIdCollection as $item) {
                                $targetEntityObject = new $targetEntity();
                                $this->deserialized($entityManager, $item, $targetEntityObject);
                                $this->getAddToCollectionMethod($model, $key);
                                if (method_exists($model, $this->getAddToCollectionMethod($model, $key))) {
                                    $model->{$this->getAddToCollectionMethod($model, $key)}($targetEntityObject);
                                }
                            }
                        } else {
                            if (method_exists($model, 'set' . ucfirst($key))) {
                                $assoMapping = $metadata->getAssociationMapping($key);
                                $targetEntity = $assoMapping['targetEntity'];
                                $targetEntityObject = new $targetEntity();
                                if ($value) {
                                    $targetEntityObject = $this->deserialized($entityManager, $value, $targetEntityObject);
                                    $model->{'set' . ucfirst($key)}($targetEntityObject);
                                }
                            }
                        }
                    }

                    continue;
                }

                if (method_exists($model, 'set' . ucfirst($key))) {
                    try {
                        $model->{'set' . ucfirst($key)}($value);
                    } catch (\TypeError $exception) {
                    }
                }
            }
        }

        $entityDeserializerEvent = new EntityDeserializerEvent($data, $model);
        $this->eventDispatcher->dispatch(EntityDeserializerEvent::EVENT_NAME, $entityDeserializerEvent);

        return $model;
    }

    /**
     * Found & get add collection method
     *
     * @param mixed|VWorkflowTrait $model
     * @param string               $property
     * @return string
     * @throws \Exception
     */
    public function getAddToCollectionMethod($model, $property)
    {
        $property = ucfirst($property);
        $check = [];
        $transform = ['ies' => 'y', 's' => '', 'List' => ''];
        foreach ($transform as $collection => $item) {
            if (preg_match('/' . $collection . '$/', $property)) {
                $propertyMethod = preg_replace('/' . $collection . '$/', $item, $property);
                if (method_exists($model, 'add' . $propertyMethod)) {
                    return 'add' . $propertyMethod;
                }

                $check[] = 'add' . $propertyMethod . '()';
            }
        }

        if (method_exists($model, 'add' . ucfirst($property))) {
            return 'add' . ucfirst($property);
        }

        $check[] = 'add' . ucfirst($property) . '()';

        throw new \Exception('Not method add for collection is found : ' . implode(', ', $check));
    }
}
