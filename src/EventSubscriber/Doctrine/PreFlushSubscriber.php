<?php
/**
 * PreFlushSubscriber
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber\Doctrine;

use Coosos\VWorkflowBundle\Entity\VWorkflow;
use Coosos\VWorkflowBundle\EventSubscriber\AbstractSubscriber;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreFlushEventArgs;

/**
 * Class PreFlushSubscriber
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber\Doctrine
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class PreFlushSubscriber extends AbstractSubscriber implements EventSubscriber
{
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return ['preFlush'];
    }

    /**
     * Check if persist original model
     *
     * @param PreFlushEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $inserts = $args->getEntityManager()->getUnitOfWork()->getScheduledEntityInsertions();
        foreach ($inserts as $insert) {
            if (!$insert instanceof VWorkflow) {
                continue;
            }

            if ($insert->getObjectDeserialized()) {
                $args->getEntityManager()->persist($insert->getObjectDeserialized());
            }
        }
    }
}
