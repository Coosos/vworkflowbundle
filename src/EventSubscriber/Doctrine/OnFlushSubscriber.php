<?php
/**
 * OnFlushSubscriber
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber\Doctrine;

use Coosos\VWorkflowBundle\EventSubscriber\AbstractSubscriber;
use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Coosos\VWorkflowBundle\Service\VWorkflow\ClassContains;
use Coosos\VWorkflowBundle\Service\VWorkflow\Configuration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\PersistentCollection;

/**
 * Class OnFlushSubscriber
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber\Doctrine
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class OnFlushSubscriber extends AbstractSubscriber implements EventSubscriber
{
    /**
     * @var Configuration configuration
     */
    private $configuration;

    /**
     * @var array
     */
    private $detachDeletionsHash = [];

    /**
     * OnFlushSubscriber constructor.
     *
     * @param ClassContains $classContains
     * @param Configuration $configuration
     */
    public function __construct(ClassContains $classContains, Configuration $configuration)
    {
        parent::__construct($classContains);
        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return ['onFlush'];
    }

    /**
     * Detach entity if is vworkflow (not merge)
     *
     * @param OnFlushEventArgs $args
     * @throws \ReflectionException
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $unitOfWork = $args->getEntityManager()->getUnitOfWork();
        $entityFlushList = [
            'entityUpdates' => $unitOfWork->getScheduledEntityUpdates(),
            'entityInsertions' => $unitOfWork->getScheduledEntityInsertions(),
            'entityDeletions' => $unitOfWork->getScheduledEntityDeletions(),
            'collectionUpdates' => $unitOfWork->getScheduledCollectionUpdates(),
            'collectionDeletions' => $unitOfWork->getScheduledCollectionDeletions(),
        ];

        foreach ($entityFlushList as $scheduledType => $item) {
            if ($scheduledType === 'entityUpdates' || $scheduledType === 'entityInsertions') {
                foreach ($item as $scheduledEntity) {
                    if ($this->classContains->hasTrait($scheduledEntity, VWorkflowTrait::class)
                        && !is_null($scheduledEntity->workflowName)
                        && $this->configuration->getByWorkflowName($scheduledEntity->workflowName)) {
                        /** @var VWorkflowTrait $scheduledEntity */

                        $vworkflowConfiguration = $this->configuration->getByWorkflowName($scheduledEntity->workflowName);
                        $autoMerge = false;

                        if ($scheduledEntity->getMarking()) {
                            if (is_array($scheduledEntity->getMarking())) {
                                foreach ($scheduledEntity->getMarking() as $status => $value) {
                                    if ($vworkflowConfiguration->isStatusAutoMerge($status)) {
                                        $autoMerge = true;
                                        break;
                                    }
                                }
                            } elseif (is_string($scheduledEntity->getMarking())) {
                                if ($vworkflowConfiguration->isStatusAutoMerge($scheduledEntity->getMarking())) {
                                    $autoMerge = true;
                                }
                            }
                        }

                        if (!$autoMerge) {
                            $this->detachRecursive($args, $scheduledEntity);
                        }
                    }
                }
            }

            if ($scheduledType === 'entityDeletions' || $scheduledType === 'collectionDeletions') {
                foreach ($item as $scheduledEntity) {
                    $this->detachDeletionsHash = [];
                    $detachDeletions = $this->checkDetachDeletionsRecursive($args, $scheduledEntity);
                    if ($detachDeletions && !$scheduledEntity instanceof PersistentCollection) {
                        $args->getEntityManager()->persist($scheduledEntity);
                        $this->detachRecursive($args, $scheduledEntity);
                    }

                    if ($detachDeletions && $scheduledEntity instanceof PersistentCollection) {
                        $mapping = $scheduledEntity->getMapping();
                        $mapping['orphanRemoval'] = false;
                        $scheduledEntity->setOwner($scheduledEntity->getOwner(), $mapping);
                    }
                }
            }
        }

        if ($unitOfWork->getScheduledEntityInsertions()) {
            foreach ($unitOfWork->getScheduledEntityInsertions() as $entityInsertion) {
                if (property_exists($entityInsertion, 'workflowDetach') && $entityInsertion->{'workflowDetach'}) {
                    $args->getEntityManager()->detach($entityInsertion);
                }
            }
        }
    }

    /**
     * Check if relation use object detached
     */
    protected function checkDetachDeletionsRecursive(OnFlushEventArgs $args, $entity)
    {
        if (is_null($entity)) {
            return false;
        }

        if (property_exists($entity, 'workflowDetach') && $entity->workflowDetach) {
            return true;
        }

        if (in_array(spl_object_hash($entity), $this->detachDeletionsHash)) {
            return false;
        }

        $this->detachDeletionsHash[] = spl_object_hash($entity);

        if ($entity instanceof PersistentCollection) {
            $isDetach = $this->checkDetachDeletionsRecursive($args, $entity->getOwner());
            if ($isDetach) {
                return true;
            }
        }

        $meta = $args->getEntityManager()->getClassMetadata(get_class($entity));
        foreach ($meta->getAssociationMappings() as $fieldName => $associationMapping) {
            if ($entity->{'get' . ucfirst($fieldName)}() instanceof Collection) {
                // TODO
            } else {
                $isDetach = $this->checkDetachDeletionsRecursive($args, $entity->{'get' . ucfirst($fieldName)}());
                if ($isDetach) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Recursive detach
     *
     * @param OnFlushEventArgs     $args
     * @param VWorkflowTrait|mixed $entity
     */
    private function detachRecursive(OnFlushEventArgs $args, $entity)
    {
        $args->getEntityManager()->detach($entity);
        $entity->workflowDetach = true;
        $classMetaData = $args->getEntityManager()->getClassMetadata(get_class($entity));
        foreach ($classMetaData->getAssociationMappings() as $key => $associationMapping) {
            if ($entity->{'get' . ucfirst($key)}() instanceof PersistentCollection) {
                /** @var PersistentCollection $getCollectionMethod */
                $getCollectionMethod = $entity->{'get' . ucfirst($key)}();
                foreach ($getCollectionMethod as $item) {
                    if (property_exists($item, 'workflowDetach') && $item->workflowDetach) {
                        continue;
                    }

                    $this->detachRecursive($args, $item);

                    continue;
                }

                /** @var PersistentCollection $tags */
                $mapping = $getCollectionMethod->getMapping();
                $mapping['isOwningSide'] = false;
                $getCollectionMethod->setOwner($entity, $mapping);
            } elseif (!($entity->{'get' . ucfirst($key)}() instanceof ArrayCollection)) {
                $item = $entity->{'get' . ucfirst($key)}();
                if (!is_null($item)) {
                    if (property_exists($item, 'workflowDetach') && $item->workflowDetach) {
                        continue;
                    }

                    $this->detachRecursive($args, $item);
                }

                continue;
            }
        }
    }
}
