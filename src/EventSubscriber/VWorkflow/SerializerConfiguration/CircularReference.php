<?php
/**
 * CircularReference
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber\VWorkflow\SerializerConfiguration;

use Coosos\VWorkflowBundle\Event\SerializerConfigurationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CircularReference
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber\VWorkflow\SerializerConfiguration
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class CircularReference implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [SerializerConfigurationEvent::EVENT_NAME => 'serializerConfiguration'];
    }

    /**
     * Circular reference
     *
     * @param SerializerConfigurationEvent $event
     */
    public function serializerConfiguration(SerializerConfigurationEvent $event)
    {
        $normalizer = $event->getNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            /** @var mixed $object */
            return $object->getId();
        });
    }
}
