<?php
/**
 * Datetime
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber\VWorkflow\SerializerConfiguration;

use Coosos\VWorkflowBundle\Event\SerializerConfigurationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class Datetime
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber\VWorkflow\SerializerConfiguration
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class Datetime implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [SerializerConfigurationEvent::EVENT_NAME => 'serializerConfiguration'];
    }

    /**
     * Convert datetime
     *
     * @param SerializerConfigurationEvent $event
     */
    public function serializerConfiguration(SerializerConfigurationEvent $event)
    {
        $normalizer = $event->getNormalizer();

        $callbackDatetime = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format(\DateTime::ISO8601)
                : '';
        };

        $normalizer->setCallbacks(['createdAt' => $callbackDatetime, 'updatedAt' => $callbackDatetime]);
    }
}
