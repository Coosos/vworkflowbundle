<?php
/**
 * AbstractSubscriber
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\EventSubscriber;

use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Coosos\VWorkflowBundle\Service\VWorkflow\ClassContains;

/**
 * Class AbstractSubscriber
 *
 * @package Coosos\VWorkflowBundle\EventSubscriber
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
abstract class AbstractSubscriber
{
    /**
     * @var ClassContains classContains
     */
    protected $classContains;

    /**
     * AbstractSubscriber constructor.
     *
     * @param ClassContains $classContains
     */
    public function __construct(ClassContains $classContains)
    {
        $this->classContains = $classContains;
    }

    /**
     * @param mixed $model
     * @return bool
     * @throws \ReflectionException
     */
    public function hasVWorkflowTrait($model)
    {
        return $this->classContains->hasTrait($model, VWorkflowTrait::class);
    }
}
