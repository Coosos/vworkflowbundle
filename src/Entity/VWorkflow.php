<?php
/**
 * VWorkflow.php
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Entity;

use Coosos\VWorkflowBundle\Model\Entity\VWorkflowTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VWorkflow
 *
 * @package Coosos\VWorkflowBundle\Entity
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 * @ORM\Entity(repositoryClass="Coosos\VWorkflowBundle\Repository\VWorkflowRepository")
 */
class VWorkflow
{
    /**
     * @var int id
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string entityClass
     * @ORM\Column(type="string", length=255)
     */
    private $entityClass;

    /**
     * @var string instance
     * @ORM\Column(type="string", length=255)
     */
    private $instance;

    /**
     * @var bool isMerged
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $isMerged;

    /**
     * @var array|null marking
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $marking;

    /**
     * @var string objectSerialized
     * @ORM\Column(type="text")
     */
    private $objectSerialized;

    /**
     * @var string workflowName
     * @ORM\Column(type="string", length=255)
     */
    private $workflowName;

    /**
     * @var VWorkflow old
     * @ORM\ManyToOne(targetEntity="Coosos\VWorkflowBundle\Entity\VWorkflow")
     */
    private $old;

    /**
     * @var mixed|VWorkflowTrait objectDeserialized
     */
    private $objectDeserialized;

    /**
     * VWorkflow constructor.
     */
    public function __construct()
    {
        $this->isMerged = false;
    }

    /**
     * @return string
     */
    public function getObjectSerialized()
    {
        return $this->objectSerialized;
    }

    /**
     * @param string $objectSerialized
     * @return self
     */
    public function setObjectSerialized(string $objectSerialized): VWorkflow
    {
        $this->objectSerialized = $objectSerialized;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * @param string $entityClass
     * @return self
     */
    public function setEntityClass(string $entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param string $instance
     * @return self
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMerged()
    {
        return $this->isMerged;
    }

    /**
     * @param bool $isMerged
     * @return self
     */
    public function setIsMerged(bool $isMerged)
    {
        $this->isMerged = $isMerged;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getMarking()
    {
        return $this->marking;
    }

    /**
     * @param array|null $marking
     */
    public function setMarking($marking)
    {
        $this->marking = $marking;
    }

    /**
     * @return VWorkflow|null
     */
    public function getOld()
    {
        return $this->old;
    }

    /**
     * @param VWorkflow|null $old
     * @return VWorkflow
     */
    public function setOld($old)
    {
        $this->old = $old;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkflowName()
    {
        return $this->workflowName;
    }

    /**
     * @param string $workflowName
     * @return self
     */
    public function setWorkflowName(string $workflowName): self
    {
        $this->workflowName = $workflowName;

        return $this;
    }

    /**
     * @return VWorkflowTrait|mixed
     */
    public function getObjectDeserialized()
    {
        return $this->objectDeserialized;
    }

    /**
     * @param VWorkflowTrait|mixed $objectDeserialized
     * @return VWorkflow
     */
    public function setObjectDeserialized($objectDeserialized)
    {
        $this->objectDeserialized = $objectDeserialized;

        return $this;
    }
}
