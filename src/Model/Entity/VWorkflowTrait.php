<?php
/**
 * VWorkflowTrait.php
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Model\Entity;

use Coosos\VWorkflowBundle\Entity\VWorkflow;
use Coosos\VWorkflowBundle\Model\VWorkflow\VWorkflowAction;
use Doctrine\ORM\Mapping as ORM;

trait VWorkflowTrait
{
    /**
     * Contains VWorkflow model if entity get by VWorkflow entity
     *
     * @var VWorkflow|null vworkflowModel
     */
    public $vworkflowModel = null;

    /**
     * @var string workflowName
     */
    public $workflowName;

    /**
     * @var array|string|null marking
     */
    public $marking;

    /**
     * @var string workflowAction
     */
    public $workflowAction = VWorkflowAction::VWORKFLOW_AUTO;

    /**
     * @var string|null vworkflowUpdated
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $vworkflowUniqId;

    /**
     * @return null|VWorkflow
     */
    public function getVworkflowModel()
    {
        return $this->vworkflowModel;
    }

    /**
     * @param VWorkflow $vworkflowModel
     * @return self
     */
    public function setVworkflowModel($vworkflowModel)
    {
        $this->vworkflowModel = $vworkflowModel;

        return $this;
    }

    /**
     * @return array|string|null
     */
    public function getMarking()
    {
        return $this->marking;
    }

    /**
     * @param array|string $marking
     * @return self
     */
    public function setMarking($marking)
    {
        $this->marking = $marking;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVworkflowUniqId()
    {
        return $this->vworkflowUniqId;
    }

    /**
     * @return self
     * @deprecated
     */
    public function vworkflowenerateUniqId(): self
    {
        return $this->vworkflowGenerateUniqId();
    }


    /**
     * @return self
     */
    public function vworkflowGenerateUniqId(): self
    {
        $this->vworkflowUniqId = uniqid();

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkflowName()
    {
        return $this->workflowName;
    }

    /**
     * @param string $workflowName
     * @return self
     */
    public function setWorkflowName(string $workflowName)
    {
        $this->workflowName = $workflowName;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkflowAction()
    {
        return $this->workflowAction;
    }

    /**
     * @param string $workflowAction
     * @return self
     */
    public function setWorkflowAction(string $workflowAction)
    {
        $this->workflowAction = $workflowAction;

        return $this;
    }
}
