<?php
/**
 * WorkflowConfiguration
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Model\Configuration;

/**
 * Class WorkflowConfiguration
 *
 * @package Coosos\VWorkflowBundle\Model\Configuration
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class WorkflowConfiguration
{
    /**
     * @var string name
     */
    private $name;

    /**
     * @var array autoMerge
     */
    private $autoMerge;

    /**
     * WorkflowConfiguration constructor.
     *
     * @param string $name
     * @param array  $workflow
     */
    public function __construct($name, array $workflow)
    {
        $this->autoMerge = [];
        $this->name = $name;

        if (isset($workflow['auto_merge'])) {
            foreach ($workflow['auto_merge'] as $item) {
                $this->addAutoMergeStatus($item);
            }
        }
    }

    /**
     * @param string $status
     */
    public function addAutoMergeStatus($status)
    {
        $this->autoMerge[] = $status;
    }

    /**
     * @param string $status
     * @return bool
     */
    public function isStatusAutoMerge(string $status)
    {
        return in_array($status, $this->autoMerge);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
