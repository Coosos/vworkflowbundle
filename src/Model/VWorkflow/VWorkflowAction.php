<?php
/**
 * VWorkflowAction
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Model\VWorkflow;

/**
 * Class VWorkflowAction
 *
 * @package Coosos\VWorkflowBundle\Model\VWorkflow
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class VWorkflowAction
{
    /**
     * Run the default bundle processing
     */
    const VWORKFLOW_AUTO = 'auto';

    /**
     * Disable versioning
     */
    const VWORKFLOW_DISABLE = 'disable';

    /**
     * Force to merge
     */
    const VWORKFLOW_FORCE = 'force';
}
