<?php
/**
 * VWorkflowRepository
 *
 * @category Coosos
 * @package  Coosos\VWorkflowBundle
 * @author   Remy LESCALLIER <lescallier1@gmail.com>
 */

namespace Coosos\VWorkflowBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * Class VWorkflowRepository
 *
 * @package Coosos\VWorkflowBundle\Repository
 * @author  Remy LESCALLIER <lescallier1@gmail.com>
 */
class VWorkflowRepository extends EntityRepository
{
    /**
     * Get list
     *
     * @param string $classPath
     * @param string $workflowName
     * @param string $marking
     * @param bool   $lastOfInstance
     * @return mixed
     */
    public function getList(string $classPath, string $workflowName, $marking, $lastOfInstance = true)
    {
        $queryBuilder = $this->createQueryBuilder('vw');

        $queryBuilder->andWhere($queryBuilder->expr()->eq('vw.entityClass', ':entityClass'))
                     ->setParameter(':entityClass', $classPath);

        $queryBuilder->andWhere($queryBuilder->expr()->eq('vw.workflowName', ':workflowName'))
                     ->setParameter(':workflowName', $workflowName);

        if ($marking) {
            $queryBuilder->andWhere($queryBuilder->expr()->like('vw.marking', ':marking'))
                         ->setParameter(':marking', '%' . $marking . '%');
        } else {
            $queryBuilder->andWhere($queryBuilder->expr()->isNull('vw.marking'));
        }

        if ($lastOfInstance) {
            $queryBuilderNotIn = $this->createQueryBuilder('vw2')
                                      ->select('IDENTITY(vw2.old)')
                                      ->andWhere('IDENTITY(vw2.old) = vw.id')
                                      ->getQuery();

            $queryBuilder->andWhere($queryBuilder->expr()->notIn('vw.id', $queryBuilderNotIn->getDQL()));
        }


        return $queryBuilder->getQuery()->getResult();
    }
}
