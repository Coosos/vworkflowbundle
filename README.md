# Coosos/VworkflowBundle

**/!\ This bundle is currently under development /!\\**

If you wish, you can contribute to the project :)

## Description

This bundle uses the Symfony Workflow bundle to track transitions in another table. 

Once all transitions are complete, it merges the object into the original table. 

If an old entity yet exists the same identifier, it will be replaced while keeping the same identifier. 

**This can be very useful for avoiding a loss of SEO from search engines.**

## Installation & Configuration

* Step 1 : [Installation](/doc/install.md)
* Step 2 : [Configuration](/doc/config.md)

## Use Bundle

* Step 3 : [Apply model](/doc/apply.md)
* Step 4 : *Get model*
* Step 5 : *Event*

# Author

* Author : Rémy Lescallier
