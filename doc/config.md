# Configuration

**Example to use**

    coosos_v_workflow:
        workflows:
            news_workflow:
                auto_merge:
                    - publish
            other_workflow:
                auto_merge:
                    - solved
                    - close


*news_workflow* and *other_workflow* : This is the name of the workflow specified in **framework.workflows**

*auto_merge* : Is the key to indicate that if the object with the status indicated in the list then it is merge in the 
original table
