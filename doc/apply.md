# Apply model

## Apply entity

...

## Apply entity : Example

**Create draft article**
    
    $article = new Article();
    $article->setTitle('Hello world');

    $workflowService = $this->get('workflow.article');
    $vworkflow = $this->get('coosos.v_workflow')->apply($article, $workflowService);
    $entityManager->persist($vworkflow);
    $entityManager->flush();

**Change transition article : to_validation & to_publish**

    ...
    $workflowService = $this->get('workflow.article');
    $vworkflow = $this->get('coosos.v_workflow')->apply(
        $article, 
        $workflowService,
        ['to_validation', 'to_publish']
    );
    
    $entityManager->persist($vworkflow);
    $entityManager->flush();
    ...