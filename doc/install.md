# Installation

## Step 1 : Install bundle

Use composer for install bundle in your project :

    $ composer req coosos/vworkflow-bundle
    
## Step 2 : Enable bundle

**For standard structure**

    // app/AppKernel.php
    // ...
    class AppKernel extends Kernel
    {
        public function registerBundles()
        {
            $bundles = array(
                // ...
                new Coosos\VWorkflowBundle\CoososVWorkflowBundle(),
            );
            // ...
        }
        // ...
    }

**For flex structure**

    // config/bundles.php

    return [
        // ...
        Coosos\VWorkflowBundle\CoososVWorkflowBundle::class => ['all' => true],
    ];


## Step 3 : Install new table in your database

    $ php bin/console doctrine:schema:update -f
